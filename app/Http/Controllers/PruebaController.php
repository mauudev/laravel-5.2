<?php

namespace Cinema\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PruebaController extends BaseController{
   public function index(){
        return "<h2>Hola desde controller seee !";
   }
    public function salchichon($nombre = 'jhon.. salchichon'){
        return "<h2>Mi nombre es ".$nombre;
    }
}
