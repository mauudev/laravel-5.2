<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::get('controlador','PruebaController@index');
Route::get('salchichon/{nombre?}','PruebaController@salchichon');
Route::resource('movie','MovieController');//RESTFul
Route::get('prueba',function(){//clasico
    return "<h1>Hola desde routes</h1>";
});
Route::get('nombre/{nombre}',function($nombre){//parametro
    return "<h1>Mi nombre es: ".$nombre;
});
Route::get('edad/{edad}',function($edad){//parametro
    return "<h1>Mi edad es: ".$edad;
});
Route::get('edad2/{edad?}',function($edad=26){//valor por default
    return "<h1>Mi edad es: ".$edad;
});
Route::group(['middleware' => ['web']], function () {
    //
});
